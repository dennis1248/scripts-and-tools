## Devserver
This script is used to quickly launch and quit Nginx and Mariadb. 


## Usage
### Install
- Run `./install.sh`.

### Uninstall
- Run `./install.sh uninstall`

### Commands
| Command        	| Description      	|
| ------------- 	|-------------		|
| `devserver`     	| Start services 	|
| `devserver stop`	| Stop services 	|