#!/bin/bash

if [ "$EUID" -ne 0 ]
then 
	echo "This script has to be run as root"
  exit
fi

if [ "$1" == "uninstall" ];
then
	rm -v /usr/bin/devserver
	echo "Devserver has been removed"
else 
	cp -v ./devserver /usr/bin/devserver
	chmod +x /usr/bin/devserver
	echo "Devserver has been installed"
fi

