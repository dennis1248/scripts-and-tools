## usage
1. Copy `backup.service` in to `/etc/systemd/system`
2. Run `systemctl daemon-reload` to reload the systemd manager configuration.
3. Enable the service with `systemctl enable backup.service` to run it automatically on boot
4. Start the service with `systemctl start backup.service`

## What does it do?
All it does is copy your personal folders and dot files from your home directory. You will want to edit the script to set the correct file paths or else the service will not work. 
